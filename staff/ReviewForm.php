<?php

namespace App\Http\Requests;

use App\Review;
use Illuminate\Foundation\Http\FormRequest;

class ReviewForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $review = new Review();

        if (empty($this->input('ending'))) {
            $this->merge(['ending' => null]);
        }

        $currentYear = (int)date('Y');

        return array_merge($this->getProfileSpecificRules(), [
            'ending' => 'nullable|integer|between:' . ($currentYear - 9) . ',' . $currentYear,
            'length' => 'required|in:' . $review->getLengthOfEmploymentString(),
            'status' => 'required|in:' . $review->getEnumString('status'),
            'position' => 'required|string|min:3|max:64',
            'review' => 'required|string|min:100|max:2000|censored',
            'advice' => 'nullable|string|min:10|max:2000|censored',
            'ratings' => 'required|size:10',
            'ratings.*' => 'required|integer|between:1,5',
        ]);
    }

    /**
     * Get profile specified rules.
     *
     * @return array
     */
    protected function getProfileSpecificRules(): array
    {
        switch (\Route::currentRouteName()) {
            case 'companies.reviews.store':
                return array_merge(
                    ['company_id' => 'sometimes|integer|exists:companies,id'],
                    CompanyForm::requiredRules()
                );
            case 'employees.reviews.store':
                return array_merge(
                    ['employee_id' => 'sometimes|integer|exists:employees,id'],
                    EmployeeForm::requiredRules()
                );
            default:
                return [];
        }
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator): void
    {
        if (\Route::currentRouteName() === 'companies.reviews.store') {
            $companyForm = new CompanyForm($this->only(['locality', 'country']));
            $companyForm->withValidator($validator);
        }
    }
}
