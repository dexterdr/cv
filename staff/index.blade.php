@extends('default')

@section('content')
    <div class="card">
        <h5 class="card-header">{{ $employee->firstname }} {{ $employee->lastname }}</h5>
        <div class="card-body">
            <p>
                <strong>@lang('common.verified'):</strong>
                {{ $employee->verified ? __('common.yes') : __('common.no') }}
            </p>

            <div class="text-right">
                <a href="{{ route('employees.show', $employee->id) }}" class="btn btn-primary">
                    <i class="material-icons">person</i>
                    @lang('common.profile')
                </a>
            </div>
        </div>
    </div>

    <div class="card mt-grid">
        <div class="card-body">
            @if ($reviews->count())
                <ul class="list-unstyled">
                    @foreach ($reviews as $review)
                        @include('reviews.employees.single')
                    @endforeach
                </ul>

                {{ $reviews->links() }}
            @else
                <div class="alert alert-warning last">@lang('common.empty')</div>
                @if (Auth::check() && Auth::user()->isCompany())
                    <div class="text-center mt-4">
                        <a href="{{ route('companies.reviews.create', ['id' => $employee->id]) }}" class="btn btn-info">
                            <i class="material-icons">add</i>
                            @lang('reviews.create')
                        </a>
                    </div>
                @endif
            @endif
        </div>
    </div>
@endsection
