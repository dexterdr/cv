<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\ReviewForm;
use App\Jobs\RecalculateRating;
use App\Notifications\NewReview;
use App\Review;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class EmployeeReviewController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * The is no null pointer exception in closure because auth middleware.
     * @noinspection NullPointerExceptionInspection
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);

        $this->middleware(static function ($request, $next) {
            $user = \Auth::user();

            if (!$user->hasProfile()) {
                return redirect()->route('users.profile');
            }

            if ($user->isEmployee()) {
                abort(403);
            }

            return $next($request);
        }, ['except' => ['index', 'edit', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Employee $employee
     *
     * @return View
     */
    public function index(Employee $employee): View
    {
        $isAdmin = \Gate::allows('permission', 'admin.company');

        $reviews = $employee->getReviews();

        return view('reviews.employees.index', compact('employee', 'reviews', 'isAdmin'));
    }

    /**
     * Show the form for creating a new employee review.
     *
     * @param Request $request
     *
     * @return View
     */
    public function create(Request $request): View
    {
        if ($employee_id = $request->query('id')) {
            $employee = Employee::findOrFail($employee_id);
        } else {
            $employee = new Employee();
        }

        $review = new Review();

        return view('reviews.employees.create', compact('employee', 'review'));

    }

    /**
     * Store a newly created employee review in DB.
     *
     * @param ReviewForm $request
     *
     * @return RedirectResponse
     */
    public function store(ReviewForm $request): RedirectResponse
    {
        $employee = $this->findOrCreateEmployee(
            $request->input('employee_id'),
            $request->input('firstname'),
            $request->input('lastname'),
            $request->input('birthday')
        );

        $user_id = \Auth::id();
        if ($employee->isReviewedByUser($user_id)) {
            return redirect()->back()->withInput()->withErrors(['exists' => $employee->id]);
        }

        $review = new Review();
        $review->user_id = $user_id;
        $review->type = 'TYPE_EMPLOYEE';
        $review->profile_id = $employee->id;
        $review->fill($request->only(['ending', 'length', 'status', 'position', 'review', 'advice', 'ratings']));
        $review->save();

        RecalculateRating::dispatch($employee);

        if ($employee->user && $employee->user->newsletter) {
            $employee->user->notify(new NewReview($employee));
        }

        return redirect()->route('employees.show', $employee->id);
    }

    /**
     * Display the specified resource.
     *
     * TODO: Maybe we don't need to display a single review. The terms of reference must be clarified.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id): \Illuminate\Http\Response
    {
        return \Response::make($id);
    }

    /**
     * Show the form for editing the specified employee review.
     *
     * @param Review $review
     *
     * @return View
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Review $review): View
    {
        $this->authorize('own', $review);

        $employee = $review->employee;

        return view('reviews.employees.edit', compact('review', 'employee'));
    }

    /**
     * Update the specified employee review in DB.
     *
     * @param ReviewForm $request
     * @param Review $review
     *
     * @return RedirectResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(ReviewForm $request, Review $review): RedirectResponse
    {
        $this->authorize('own', $review);

        $review->fill($request->only(['ending', 'length', 'status', 'position', 'review', 'advice', 'ratings']));
        $review->save();

        RecalculateRating::dispatch($review->employee);

        return redirect()->route('employees.show', $review->employee->id);
    }

    /**
     * Remove the specified employee review from DB.
     *
     * @param Review $review
     *
     * @return RedirectResponse
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function destroy(Review $review): RedirectResponse
    {
        $this->authorize('own', $review);

        $employee_id = $review->profile_id;
        $review->delete();

        return redirect()->route('employees.show', $employee_id);

    }

    /**
     * Find employee by ID and main fields or create new one.
     *
     * @param  int $employee_id
     * @param  string $firstname
     * @param  string $lastname
     * @param  string $birthday
     *
     * @return Employee
     */
    protected function findOrCreateEmployee($employee_id, $firstname, $lastname, $birthday): Employee
    {
        if ($employee_id && $employee = Employee::find($employee_id)) {
            return $employee;
        }

        $employee = Employee::search($firstname, $lastname, $birthday);

        if (!$employee) {
            $employee = new Employee();
            $employee->firstname = $firstname;
            $employee->lastname = $lastname;
            $employee->birthday = $birthday;
            $employee->save();
        }

        return $employee;
    }
}
