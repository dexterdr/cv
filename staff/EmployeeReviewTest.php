<?php

namespace Tests\Feature;

use Tests\ReviewTestCase;
use App\Employee;
use App\User;
use App\Company;
use App\Review;

class EmployeeReviewTest extends ReviewTestCase
{
    /**
     * Display a listing of the employee reviews.
     */
    public function testIndex(): void
    {
        $employee = factory(Employee::class)->create();
        $route = route('employees.reviews.index', $employee->id);

        $this->get($route)->assertOk()->assertSeeTextInOrder([$employee->title, __('common.empty')]);

        config(['app.per_page' => 1]);
        $this->createEmployeeReview($employee->id);
        $this->createEmployeeReview($employee->id);

        $this->get($route)->assertOk()->assertSeeText('keyboard_arrow_right');
    }

    /**
     * Show the form for creating a new employee review.
     */
    public function testCreate(): void
    {
        $route = route('employees.reviews.create');

        $this->get($route)->assertRedirect(route('login'));

        $user = factory(User::class)->create();
        $this->actingAs($user);
        $this->get($route)->assertRedirect(route('users.profile'));

        $user->type = 'TYPE_EMPLOYEE';
        $this->get($route)->assertStatus(403);

        $company = factory(Company::class)->create(['user_id' => $user->id]);
        $user->assignCompany($company->id);
        $this->get($route)->assertOk()->assertSeeText(__('reviews.create'));

        $this->get(route('employees.reviews.create', ['id' => 1]))->assertNotFound();
    }

    /**
     * Store a newly created employee review in DB.
     */
    public function testStore(): void
    {
        $route = route('employees.reviews.store');
        $user = $this->setupUser();

        // Check data validation errors
        $this->post($route, ['employee_id' => mt_rand(), 'ending' => 1970])
            ->assertSessionHasErrors([
                'employee_id', 'firstname', 'lastname', 'birthday',
                'ending', 'length', 'status', 'position', 'review', 'ratings'
            ]);

        // Create review right way
        $employeeData = $this->getFakeEmployeeFields();
        $formData = $this->getFakeReviewFields();
        $ratingsData = $this->getFakeEmployeeRatingFields();
        $requestData = array_merge($employeeData, $formData, $ratingsData);

        \Queue::fake();
        $response = $this->post($route, $requestData);
        \Queue::assertPushed(\App\Jobs\RecalculateRating::class);

        $employee = Employee::first();
        $this->assertNotNull($employee);

        $this->assertDatabaseHas('reviews', array_merge([
            'user_id' => $user->id,
            'type' => 'TYPE_EMPLOYEE',
            'profile_id' => $employee->id
        ], $formData));

        $response->assertRedirect(route('employees.show', $employee->id));

        // Test for duplicate
        $this->post($route, $requestData)->assertSessionHasErrors('exists');
    }

    /**
     * Show the form for editing the specified employee review.
     */
    public function testEdit(): void
    {
        $user = factory(User::class)->create();
        $review = factory(Review::class)->create([
            'user_id' => $user->id,
            'type' => 'TYPE_EMPLOYEE',
            'profile_id' => factory(Employee::class)->create()->id,
        ]);
        $route = route('employees.reviews.edit', $review->id);

        $this->get($route)->assertRedirect(route('login'));
        $this->actingAs(factory(User::class)->create());
        $this->get($route)->assertStatus(403);
        $this->actingAs($user);
        $this->get(route('employees.reviews.edit', $review->id + 1))->assertNotFound();
        $this->get($route)->assertOk()->assertSeeText(__('reviews.edit'));
    }

    /**
     * Update the specified employee review in DB.
     */
    public function testUpdate(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $review = factory(Review::class)->create([
            'user_id' => $user->id,
            'type' => 'TYPE_EMPLOYEE',
            'profile_id' => factory(Employee::class)->create()->id,
        ]);
        $route = route('employees.reviews.update', $review->id);

        // Check data validation errors
        $this->patch($route, ['ending' => 1970])
            ->assertSessionHasErrors(['ending', 'length', 'status', 'position', 'review', 'ratings']);

        // Update review right way
        $formData = $this->getFakeReviewFields();
        $ratingsData = $this->getFakeEmployeeRatingFields();

        $this->patch($route, array_merge($formData, $ratingsData))
            ->assertRedirect(route('employees.show', $review->employee->id));

        $this->assertDatabaseHas('reviews', array_merge([
            'id' => $review->id,
        ], $formData));
    }

    /**
     * Remove the specified employee review from DB.
     */
    public function testDestroy(): void
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $review = factory(Review::class)->create([
            'user_id' => $user->id,
            'type' => 'TYPE_EMPLOYEE'
        ]);
        $route = route('employees.reviews.destroy', $review->id);

        $this->delete($route)->assertStatus(302);
        $this->assertDatabaseMissing('reviews', ['id' => $review->id]);
    }

    /**
     * Get last review.
     */
    public function testLastReview(): void
    {
        $employee = factory(Employee::class)->create();
        $this->createEmployeeReview($employee->id);

        $this->assertNotNull($employee->getLastReview());

        $this->get(route('employees.show', $employee->id))->assertOk();
    }

    /**
     * Setup user and employee profile.
     *
     * @return \App\User;
     */
    protected function setupUser(): User
    {
        $user = factory(User::class)->create();
        $this->actingAs($user);
        $company = factory(Company::class)->create(['user_id' => $user->id]);
        $user->assignCompany($company->id);

        return $user;
    }

    /**
     * Use Faker to create employee data.
     *
     * @return array
     */
    protected function getFakeEmployeeFields(): array
    {
        return [
            'firstname' => $this->faker->firstName,
            'lastname' => $this->faker->lastName,
            'birthday' => $this->faker->dateTimeBetween('-80 years', '-16 years')->format('Y-m-d'),
        ];
    }
}

