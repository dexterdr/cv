# Code samples for CV #

This is a small selection with my production code. I didn't choose anything particularly "tricky", the typical code for solving typical business tasks, but it allows to make an approximate impression of what you get at the output. For each project, I have taken several files that demonstrate the solution of a particular task.

### depotop ###

Examples related to the online store on OpenCart and automation of business processes.

* **_modules.scss** - SASS example 
* **filter.php** и **Module.php** - demonstration of a simplified modules structure (initially they occupy several screens)
* **gulpfile.js** - just an example of working with JS and Gulp
* **Jobs.php** - delayed task management class (extension for OpenCart)

### parser ###

Several files from the project for collecting pricing information (Slim 3 Framework).

* **AutomationModel.php** - a model for working with the categories of the parent site, which demonstrates the typical work with PDO
* **ebay.co.uk.xml** - an example of a configuration file that will need to be written to collect information from the new site
* **TaskController.php** - one of the core classes for task processing (slightly shortened)

### staff ###

Code from the site with reviews of companies and employees (Laravel 5).

* **EmployeeReviewController.php** - employee feedback management
* **EmployeeReviewTest.php** - feature-tests for reviews controller (PHPUnit)
* **index.blade.php** - a little example of working with Blade's template engine
* **ReviewForm.php** - sample class for form validation
