<?php

namespace App\CMS\Admin;

use ControllerExt;

abstract class Module extends ControllerExt
{
    /**
     * Save module settings and redirect to modules list (if it needed).
     *
     * @param string $module
     * @param string|null $cacheGroup
     */
    protected function saveSettings($module, $cacheGroup = null): void
    {
        if ($this->request->server['REQUEST_METHOD'] === 'POST' && $this->validateForm()) {
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting($module, $this->request->post);
            if ($cacheGroup) {
                $this->cache->clean($cacheGroup);
            }

            $this->session->data['success'] = $this->language->get('text_success');
            $this->redirect($this->url->link('extension/module', $this->_token));
        }
    }

    /**
     * Bootstrap basic module: breadcrumbs, language, variables, template.
     */
    protected function bootstrap(): void
    {
        $this->language->load($this->_path);
        $this->document->setTitle($this->language->get('heading_title'));
        $this->buildBreadcrumbs();
        $this->data['error_warning'] = array_get($this->error, 'warning', '');

        $this->setupLanguage([
            'heading_title', 'text_enabled', 'text_disabled',
            'text_content_top', 'text_content_bottom', 'text_column_left', 'text_column_right',
            'entry_layout', 'entry_position', 'entry_status', 'entry_sort_order',
            'button_save', 'button_cancel', 'button_add_module', 'button_remove',
        ]);

        $this->data['action_save'] = $this->link();
        $this->data['action_cancel'] = $this->url->link('extension/module', $this->_token);

        $this->data['modules'] = [];

        $this->load->model('design/layout');
        $this->data['layouts'] = $this->model_design_layout->getLayouts();

        $this->setTemplate($this->_path);
        $this->children = ['common/header', 'common/footer'];
    }
}
