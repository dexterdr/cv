<?php

use App\CMS\Admin\Module;

final class ControllerModuleFilter extends Module
{
    protected $_path = 'module/filter';

    public function index(): void
    {
        $this->saveSettings('filter');
        $this->bootstrap();

        if (isset($this->request->post['filter_module'])) {
            $this->data['modules'] = $this->request->post['filter_module'];
        } elseif ($this->config->get('filter_module')) {
            $this->data['modules'] = $this->config->get('filter_module');
        }

        $this->response->setOutput($this->render());
    }
}
