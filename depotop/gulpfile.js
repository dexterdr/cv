var gulp = require('gulp'),
    prefix = require('gulp-autoprefixer'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    spritesmith = require('gulp.spritesmith'),
    sprite = require('gulp-svg-sprite'),
    uglify = require('gulp-uglify');

gulp.task('scripts', function () {
    return gulp.src([
        './node_modules/cash-dom/dist/cash.js',
        './node_modules/axios/dist/axios.js',
        './node_modules/siema/dist/siema.min.js',
        './catalog/view/theme/depotop/js/*.js'
    ])
    .pipe(concat('app.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./catalog/view/theme/depotop/build'))
});

gulp.task('sprites', function () {
    var config = {
        mode: {
            css: {
                dest: 'sass',
                dimensions: true,
                sprite: '../images/sprites.svg',
                bust: false,
                render: {
                    scss: {
                        dest: '_sprites.scss'
                    }
                }
            }
        },
        svg: {
            namespaceClassnames: false,
            precision: 4
        }
    };

    return gulp.src('./catalog/view/theme/depotop/images/sprites/*.svg')
        .pipe(sprite(config))
        .pipe(gulp.dest('./catalog/view/theme/depotop'));
});

gulp.task('vendors', function () {
    var config = {
        imgName: 'images/vendors.png',
        cssName: 'sass/_vendors.scss',
        cssFormat: 'css',
        imgOpts: {
            height: 50,
            width: 50
        },
        cssOpts: {
            cssSelector: function (sprite) {
                return '.vendor-' + sprite.name;
            }
        }
    };

    return gulp.src('./catalog/view/theme/depotop/images/vendors/*.png')
        .pipe(spritesmith(config))
        .pipe(gulp.dest('./catalog/view/theme/depotop'));
});

gulp.task('styles', function () {
    return gulp.src('./catalog/view/theme/depotop/sass/app.scss')
        .pipe(sass({
            includePaths: ['./node_modules']
        }))
        .pipe(sass({
            sourcemap: false,
            outputStyle: 'compressed'
        }))
        .pipe(prefix({
            cascade: false
        }))
        .pipe(gulp.dest('./catalog/view/theme/depotop/build'))
});

gulp.task('watch', function () {
    gulp.watch('./catalog/view/theme/depotop/js/*.js', gulp.series('scripts'));
    gulp.watch('./catalog/view/theme/depotop/sass/**/*.scss', gulp.series('styles'));
});

gulp.task('default', gulp.series('scripts', 'sprites', 'styles'));
