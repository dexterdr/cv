<?php

namespace App;

use DB;
use InvalidArgumentException;

final class Jobs implements JobsInterface
{
    private const JOBS_TABLE = DB_PREFIX . 'jobs';

    /**
     * @var \DB
     */
    private $db;

    /**
     * @var string
     */
    private $queue;

    /**
     * Jobs constructor.
     *
     * @param \DB $db
     * @param string $queueName
     */
    public function __construct(DB $db, $queueName)
    {
        $this->db = $db;

        if (!$queueName = trim($queueName)) {
            throw new InvalidArgumentException('You must specify queue name!');
        }

        $this->queue = $this->db->escape($queueName);
    }

    /**
     * Add a new job to the current queue.
     *
     * @param array $payload
     *
     * @return bool
     */
    public function add(array $payload): bool
    {
        if (empty($payload) || !$payloadJSON = json_encode($payload)) {
            return false;
        }

        $sql = "INSERT INTO `" . self::JOBS_TABLE . "` SET " .
            "`queue` = '{$this->queue}', " .
            "`payload` = '{$this->db->escape($payloadJSON)}'";
        $this->db->query($sql);

        return $this->db->countAffected() === 1;
    }

    /**
     * Bulk add jobs into the current queue.
     *
     * @param array $jobs
     *
     * @return int
     */
    public function bulk(array $jobs): int
    {
        $jobsProcessed = 0;

        foreach ($jobs as $key => $payload) {
            if (!is_array($payload) || empty($payload) || !$payloadJSON = json_encode($payload)) {
                unset($jobs[$key]);
                continue;
            }

            $jobs[$key] = $this->db->escape($payloadJSON);
        }

        if (empty($jobs)) {
            return $jobsProcessed;
        }

        // Payload can be big enough, so split jobs to avoid max_allowed_packet overflow.
        foreach (array_chunk($jobs, 100) as $chunk) {
            $data = [];
            foreach ($chunk as $payloadJSON) {
                $data[] = "('{$this->queue}', '{$payloadJSON}')";
            }

            $sql = "INSERT INTO `" . self::JOBS_TABLE . "` (`queue`, `payload`) " .
                "VALUES " . implode(', ', $data);
            $this->db->query($sql);

            $jobsProcessed += $this->db->countAffected();
        }

        return $jobsProcessed;
    }

    /**
     * Get information about queue tasks status.
     *
     * @return array
     */
    public function info(): array
    {
        $sql = "SELECT COUNT(*) AS `total`, COUNT(`result`) AS `success`, " .
            "SUM(CASE WHEN `error` = 0 THEN 0 ELSE 1 END) AS `error` " .
            "FROM `" . self::JOBS_TABLE . "` WHERE `queue` = '{$this->queue}'";

        return array_map('intval', $this->db->query($sql)->row);
    }

    /**
     * Get next job to process queue.
     *
     * @return array
     */
    public function next(): array
    {
        $sql = "SELECT `id`, `payload` FROM `" . self::JOBS_TABLE . "` " .
            "WHERE `queue` = '{$this->queue}' AND `result` IS NULL AND `error` = 0 " .
            "ORDER BY `id` LIMIT 1";
        $query = $this->db->query($sql);
        if (!$query->num_rows) {
            return [];
        }

        return [
            'id' => (int)$query->row['id'],
            'payload' => json_decode($query->row['payload'], true)
        ];
    }

    /**
     * Set result for success job by ID.
     *
     * @param int $id
     * @param array $result
     *
     * @return bool
     */
    public function set($id, array $result): bool
    {
        if (!$id = (int)$id) {
            return false;
        }

        if (empty($result) || !$resultJSON = json_encode($result)) {
            return false;
        }

        $sql = "UPDATE `" . self::JOBS_TABLE . "` SET " .
            "`result` = '{$this->db->escape($resultJSON)}' " .
            "WHERE `id` = '{$id}'";
        $this->db->query($sql);

        return $this->db->countAffected() === 1;
    }

    /**
     * Mark job as containing error by ID.
     *
     * @param int $id
     *
     * @return bool
     */
    public function error($id): bool
    {
        if (!$id = (int)$id) {
            return false;
        }

        $sql = "UPDATE `" . self::JOBS_TABLE . "` SET " .
            "`error` = 1 " .
            "WHERE `id` = '{$id}'";
        $this->db->query($sql);

        return $this->db->countAffected() === 1;
    }

    /**
     * Delete single job by ID.
     *
     * @param int $id
     *
     * @return bool
     */
    public function delete($id): bool
    {
        if (!$id = (int)$id) {
            return false;
        }

        $this->db->query("DELETE FROM `" . self::JOBS_TABLE . "` WHERE `id` = '{$id}'");

        return $this->db->countAffected() === 1;
    }

    /**
     * Clear all queue jobs.
     *
     * @return int
     */
    public function clear(): int
    {
        $this->db->query("DELETE FROM `" . self::JOBS_TABLE . "` WHERE `queue` = '{$this->queue}'");

        return $this->db->countAffected();
    }

    /**
     * Reset errors for the current queue.
     *
     * @return bool
     */
    public function retry(): bool
    {
        $this->db->query("UPDATE `" . self::JOBS_TABLE . "` SET `error` = 0 WHERE `queue` = '{$this->queue}'");

        return $this->db->countAffected() > 0;
    }

    /**
     * Get full report for the queue.
     *
     * @return array
     */
    public function report(): array
    {
        $sql = "SELECT `id`, `payload`, `result`, `error`, `created_at` " .
            "FROM `" . self::JOBS_TABLE . "` " .
            "WHERE `queue` = '{$this->queue}'";
        $query = $this->db->query($sql);

        $result = [];
        foreach ($query->rows as $row) {
            $result[$row['id']] = [
                'payload' => json_decode($row['payload'], true),
                'result' => empty($row['result']) ? [] : json_decode($row['result']),
                'error' => (int)$row['error'],
                'created_at' => $row['created_at'],
            ];
        }

        return $result;
    }
}
