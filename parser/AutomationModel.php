<?php

namespace App\Models;

use Helpers\Config;
use PDO;

final class AutomationModel extends Model
{
    /**
     * French language ID at Depotop.com
     */
    private const LANGUAGE_ID = 3;

    /**
     * Get array with all categories of Depotop.com internet-store.
     *
     * @return array
     */
    public function getCategories(): array
    {
        $sql = 'SELECT cp.`category_id` AS `id`, ' .
            'GROUP_CONCAT(cd1.`name` ORDER BY cp.`level` SEPARATOR \' &gt; \') AS `title`, ' .
            'IF (MAX(c.`parent_id`), 1, 0) AS `has_parent` ' .
            'FROM `' . self::PFX . 'category_path` cp ' .
            'INNER JOIN `' . self::PFX . 'category` c ON cp.`path_id` = c.`category_id` ' .
            'INNER JOIN `' . self::PFX . 'category_description` cd1 ON c.`category_id` = cd1.`category_id` ' .
            'INNER JOIN `' . self::PFX . 'category_description` cd2 ON cp.category_id = cd2.category_id ' .
            'WHERE cd1.`language_id` = :language_id AND cd2.`language_id` = :language_id ' .
            'GROUP BY cp.`category_id` HAVING `has_parent` = 1 ORDER BY `title`';
        $stmt = $this->sdb()->prepare($sql);
        $stmt->bindValue(':language_id', self::LANGUAGE_ID, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll() ?: [];
    }

    /**
     * Get complete information about category.
     *
     * @param int $category_id
     *
     * @return array|false
     */
    public function getCategory(int $category_id)
    {
        $sql = 'SELECT `name` FROM `' . self::PFX . 'category_description` ' .
            'WHERE `category_id` = ? AND `language_id` = ?';
        $stmt = $this->sdb()->prepare($sql);
        $stmt->execute([$category_id, self::LANGUAGE_ID]);
        if (!$name = $stmt->fetchColumn()) {
            return false;
        }

        $sql = 'SELECT `platform`, `url` FROM `' . self::PFX . 'category_search` WHERE `category_id` = ?';
        $stmt = $this->sdb()->prepare($sql);
        $stmt->execute([$category_id]);

        $links = [];
        if ($rows = $stmt->fetchAll()) {
            foreach ($rows as $row) {
                if (!filter_var($row['url'], FILTER_VALIDATE_URL)) {
                    continue;
                }

                $links[$row['platform']] = $row['url'];
            }
        }

        return [
            'name' => $name,
            'links' => $links,
        ];
    }

    /**
     * Get model for the specified category and account.
     *
     * @param int $category_id
     * @param array $accounts
     *
     * @return array
     */
    public function getCategoryModels(int $category_id, array $accounts): array
    {
        if ($category_id <= 0 || empty($accounts)) {
            return [];
        }

        $sql = 'SELECT DISTINCT p.`model` FROM `' . self::PFX . 'product` p ' .
            'INNER JOIN `' . self::PFX . 'product_to_category` p2c ON p2c.`product_id` = p.`product_id` ' .
            'INNER JOIN `' . self::PFX . 'product_external` pe ON pe.`product_id` = p.`product_id` ' .
            "WHERE p2c.`category_id` = ? AND p.`status` = 1 AND pe.`store` = 'EBAY' " .
            'AND pe.`account` IN (' . implode(', ', array_fill(0, count($accounts), '?')) . ')';
        $stmt = $this->sdb()->prepare($sql);
        $stmt->execute(array_merge([$category_id], $accounts));

        return $stmt->fetchAll(PDO::FETCH_COLUMN) ?: [];
    }

    /**
     * Get associative array of account by platform name.
     *
     * @return array
     */
    public function getPlatforms(): array
    {
        $result = [];

        try {
            $platforms = Config::getInstance()->get('Platforms');
        } catch (\Exception $e) {
            return $result;
        }

        if (!isset($platforms->Account)) {
            return $result;
        }

        foreach ($platforms->Account as $account) {
            if (isset($account['name'])) {
                $result[(string)$account['name']][] = (string)$account;
            }
        }

        return $result;
    }
}
