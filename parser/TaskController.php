<?php

namespace App\Controllers;

use App\Models\SettingsModel;
use App\Models\TaskModel;
use Ds\Set;
use Helpers\Filter;
use Helpers\Parser;
use Helpers\Postprocess\Processor;
use Helpers\Sites\Site;
use Helpers\Traits\Config;
use Psr\Container\ContainerInterface;
use Slim\Http\Request;
use Slim\Http\Response;

final class TaskController extends Controller
{
    use Config;

    private const ROWS_CHUNK = 2000;

    /**
     * @var TaskModel
     */
    private $model;

    /**
     * @var Parser|null
     */
    private $parser;

    /**
     * @var Site|null
     */
    private $site;

    /**
     * TaskController constructor.
     *
     * @param ContainerInterface $ci
     */
    public function __construct(ContainerInterface $ci)
    {
        parent::__construct($ci);

        $this->model = new TaskModel($ci);
    }

    /**
     * Aggressive parsing of single sub-task.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     *
     * @noinspection PhpUnusedParameterInspection
     */
    public function start(Request $request, Response $response, array $args): Response
    {
        $task_id = (int)array_get($args, 'id', 0);
        if ($task_id === 0) {
            return $response->withJson(['error' => true]);
        }

        $row = $this->model->getNextRow($task_id);
        $isNext = false;
        if (!empty($row)) {
            $this->process($row);
            $isNext = true;
        }

        $info = $this->model->getTaskStats($task_id);
        $info['next'] = $isNext;

        return $response->withJson($info);
    }

    /**
     * Process single task row. This is a helper for start() and cron().
     *
     * @param array $task
     */
    private function process(array $task): void
    {
        $settings_id = (int)$task['settings_id'];
        if ($settings_id === 0) {
            $settings = false;
        } else {
            $settingsModel = new SettingsModel($this->ci());
            $settings = $settingsModel->getTaskSettings($settings_id);
        }

        $result = $this->parse($task['url'], $task['type'], $task['subtasks'], $settings);

        if (empty($result)) {
            $this->model->setError($task['id']);
        } else {
            $this->model->setResult($task['id'], $result);
        }
    }

    /**
     * Get CSV-report for specified task.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function report(Request $request, Response $response, array $args): Response
    {
        if ($task_id = (int)array_get($args, 'id', 0)) {
            $type = $this->model->getTaskType($task_id);

            if (!empty($type) && $filename = $this->generateListReport($task_id)) {
                return $response->withRedirect('/temp/' . $filename, 302);
            }
        }

        return $this->redirectBack($request, $response);
    }

    /**
     * Optimized list report generation.
     * DO NOT TOUCH! May be VERY resource-depended!
     *
     * @param int $task_id
     *
     * @return string|false
     */
    public function generateListReport(int $task_id)
    {
        $models = $this->model->getDistinctModelsInTask($task_id);
        if (empty($models)) {
            return false;
        }

        $csv = '';
        foreach (array_chunk($models, 10) as $chunk) {
            $query = $this->model->getReportDataByModels($task_id, $chunk);

            foreach ($query as $model => $data) {
                $unique = new Set();
                $results = [];
                $prices = [];

                foreach ($data as $row) {
                    if (!$items = @unserialize($row, ['allowed_classes' => false])) {
                        continue;
                    }

                    foreach ($items as $item) {
                        if (!isset($item['url']) || $unique->contains($item['url'])) {
                            continue;
                        }

                        $unique->add($item['url']);

                        $results[] = $item;
                        $prices[] = (float)$item['price'] + (float)$item['shipping'];
                    }
                }

                array_multisort($prices, SORT_ASC, SORT_NUMERIC, $results);
                $results = array_slice($results, 0, 5);

                $model = '"' . $model . '";"';
                foreach ($results as $row) {
                    $csv .= $model . implode('";"', $row) . "\"\n";
                }
            }
        }

        $filename = time() . '_' . $task_id . '_' . str_random(10) . '.csv';
        file_put_contents(getenv('DIR_TEMP') . $filename, $csv);

        return $filename;
    }

    /**
     * Delete specified task with all data.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     *
     * @return Response
     */
    public function delete(Request $request, Response $response, array $args): Response
    {
        $task_id = (int)array_get($args, 'id', 0);

        if ($task_id !== 0) {
            $this->model->delete($task_id);
        }

        return $this->redirectBack($request, $response);
    }

    /**
     * Redirect back to previous or to homepage if error.
     *
     * @param Request $request
     * @param Response $response
     *
     * @return Response
     */
    private function redirectBack(Request $request, Response $response): Response
    {
        $header = $request->getHeader('HTTP_REFERER');
        $url = $header ? array_shift($header) : $this->router()->pathFor('home');

        return $response->withRedirect($url, 302);
    }

    /**
     * Parse single URL.
     *
     * @param string $url
     * @param string $type
     * @param int $subtasks
     * @param \SimpleXMLElement|false $settings
     *
     * @return array|false
     */
    private function parse(string $url, string $type, int $subtasks, $settings)
    {
        if (!$config = $this->loadConfig($url)) {
            return false;
        }

        try {
            $isAuthRequired = isset($config->Auth);
            $parser = new Parser($url, $isAuthRequired);
            $site = Site::create($this->ci(), $config);

            if ($isAuthRequired && !$site->isAuthenticated($parser)) {
                if (!$site->login()) {
                    return false;
                }

                $parser = new Parser($url, true);
            }
        } catch (\Exception $e) {
            return false;
        }

        // Save parser and site instances for future use.
        $this->parser = $parser;
        $this->site = $site;

        $filter = $settings ? new Filter($settings) : null;
        $result = $parser->process($config, null, $filter, (bool)$subtasks, $type);

        return $this->postprocess($result, $config, $type);
    }

    /**
     * Postprocess parsing result (based on class, specified inside configuration).
     *
     * @param array $result
     * @param \SimpleXMLElement $config
     * @param string $type
     *
     * @return array
     */
    private function postprocess(array $result, \SimpleXMLElement $config, string $type): array
    {
        if (!isset($config->Postprocess) || empty($result)) {
            return $result;
        }

        $classname = '\Helpers\Postprocess\Processor' . $config->Postprocess;

        if (class_exists($classname)) {
            try {
                $refClass = new \ReflectionClass($classname);
                if ($refClass->isSubclassOf(Processor::class)) {
                    /** @var Processor $processor */
                    $processor = new $classname($this->ci(), $type);
                    return $processor->execute($result);
                }
            } catch (\Exception $e) { }
        }

        return $result;
    }
}
